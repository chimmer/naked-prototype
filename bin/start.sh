#!/bin/bash -e

docker pull registry.gitlab.com/fusonic/devops/images/node-ci:12.2
docker run --rm -it \
    --publish 4200-4203:4200-4203 \
    --volume $PWD:/usr/src/app \
    --volume /dev/shm:/dev/shm \
    --workdir /usr/src/app \
    --privileged \
    --name "naked" \
    registry.gitlab.com/fusonic/devops/images/node-ci:12.2 /bin/bash
