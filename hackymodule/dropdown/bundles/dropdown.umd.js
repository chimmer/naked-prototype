(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('dropdown', ['exports', '@angular/core'], factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, factory(global.dropdown = {}, global.ng.core));
}(this, (function (exports, i0) { 'use strict';

    var DropdownService = /** @class */ (function () {
        function DropdownService() {
        }
        return DropdownService;
    }());
    DropdownService.ɵfac = function DropdownService_Factory(t) { return new (t || DropdownService)(); };
    DropdownService.ɵprov = i0.ɵɵdefineInjectable({ token: DropdownService, factory: DropdownService.ɵfac, providedIn: 'root' });
    (function () {
        (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownService, [{
                type: i0.Injectable,
                args: [{
                        providedIn: 'root'
                    }]
            }], function () { return []; }, null);
    })();

    var DropdownComponent = /** @class */ (function () {
        function DropdownComponent() {
        }
        DropdownComponent.prototype.ngOnInit = function () {
        };
        return DropdownComponent;
    }());
    DropdownComponent.ɵfac = function DropdownComponent_Factory(t) { return new (t || DropdownComponent)(); };
    DropdownComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DropdownComponent, selectors: [["lib-dropdown"]], decls: 2, vars: 0, template: function DropdownComponent_Template(rf, ctx) {
            if (rf & 1) {
                i0.ɵɵelementStart(0, "p");
                i0.ɵɵtext(1, " dropdown works! ");
                i0.ɵɵelementEnd();
            }
        }, encapsulation: 2 });
    (function () {
        (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownComponent, [{
                type: i0.Component,
                args: [{
                        selector: 'lib-dropdown',
                        template: "\n    <p>\n      dropdown works!\n    </p>\n  ",
                        styles: []
                    }]
            }], function () { return []; }, null);
    })();

    var DropdownModule = /** @class */ (function () {
        function DropdownModule() {
        }
        return DropdownModule;
    }());
    DropdownModule.ɵfac = function DropdownModule_Factory(t) { return new (t || DropdownModule)(); };
    DropdownModule.ɵmod = i0.ɵɵdefineNgModule({ type: DropdownModule });
    DropdownModule.ɵinj = i0.ɵɵdefineInjector({ imports: [[]] });
    (function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DropdownModule, { declarations: [DropdownComponent], exports: [DropdownComponent] }); })();
    (function () {
        (typeof ngDevMode === "undefined" || ngDevMode) && i0.ɵsetClassMetadata(DropdownModule, [{
                type: i0.NgModule,
                args: [{
                        declarations: [DropdownComponent],
                        imports: [],
                        exports: [DropdownComponent]
                    }]
            }], null, null);
    })();

    /*
     * Public API Surface of dropdown
     */

    /**
     * Generated bundle index. Do not edit.
     */

    exports.DropdownComponent = DropdownComponent;
    exports.DropdownModule = DropdownModule;
    exports.DropdownService = DropdownService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=dropdown.umd.js.map
