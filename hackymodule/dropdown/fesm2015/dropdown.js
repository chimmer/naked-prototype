import { ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵdefineComponent, ɵɵelementStart, ɵɵtext, ɵɵelementEnd, Component, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';

class DropdownService {
    constructor() { }
}
DropdownService.ɵfac = function DropdownService_Factory(t) { return new (t || DropdownService)(); };
DropdownService.ɵprov = ɵɵdefineInjectable({ token: DropdownService, factory: DropdownService.ɵfac, providedIn: 'root' });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵsetClassMetadata(DropdownService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

class DropdownComponent {
    constructor() { }
    ngOnInit() {
    }
}
DropdownComponent.ɵfac = function DropdownComponent_Factory(t) { return new (t || DropdownComponent)(); };
DropdownComponent.ɵcmp = ɵɵdefineComponent({ type: DropdownComponent, selectors: [["lib-dropdown"]], decls: 2, vars: 0, template: function DropdownComponent_Template(rf, ctx) { if (rf & 1) {
        ɵɵelementStart(0, "p");
        ɵɵtext(1, " dropdown works! ");
        ɵɵelementEnd();
    } }, encapsulation: 2 });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵsetClassMetadata(DropdownComponent, [{
        type: Component,
        args: [{
                selector: 'lib-dropdown',
                template: `
    <p>
      dropdown works!
    </p>
  `,
                styles: []
            }]
    }], function () { return []; }, null); })();

class DropdownModule {
}
DropdownModule.ɵfac = function DropdownModule_Factory(t) { return new (t || DropdownModule)(); };
DropdownModule.ɵmod = ɵɵdefineNgModule({ type: DropdownModule });
DropdownModule.ɵinj = ɵɵdefineInjector({ imports: [[]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(DropdownModule, { declarations: [DropdownComponent], exports: [DropdownComponent] }); })();
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && ɵsetClassMetadata(DropdownModule, [{
        type: NgModule,
        args: [{
                declarations: [DropdownComponent],
                imports: [],
                exports: [DropdownComponent]
            }]
    }], null, null); })();

/*
 * Public API Surface of dropdown
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DropdownComponent, DropdownModule, DropdownService };
//# sourceMappingURL=dropdown.js.map
