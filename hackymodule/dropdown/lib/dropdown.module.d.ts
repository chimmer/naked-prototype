import * as i0 from "@angular/core";
import * as i1 from "./dropdown.component";
export declare class DropdownModule {
    static ɵfac: i0.ɵɵFactoryDef<DropdownModule, never>;
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<DropdownModule, [typeof i1.DropdownComponent], never, [typeof i1.DropdownComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<DropdownModule>;
}
//# sourceMappingURL=dropdown.module.d.ts.map