import { CdkOverlayOrigin, ConnectedPosition } from '@angular/cdk/overlay';
import { Component, ContentChild, ContentChildren, ElementRef, Input, OnInit, QueryList, ViewChild } from '@angular/core';
import { NakedOption } from './option';

@Component({
    selector: 'nkd-dropdown',
    templateUrl: 'dropdown.html',
    styleUrls: ['dropdown.scss']
})
// tslint:disable-next-line:component-class-suffix
export class NakedDropdown implements OnInit{

    @Input() text: string;

    /** The last measured value for the trigger's client bounding rect. */
    public _triggerRect: ClientRect;

    /** Whether or not the overlay panel is open. */
    private _panelOpen = false;

    @ContentChildren(NakedOption, { descendants: true }) options: QueryList<NakedOption>;

    /** Trigger that opens the select. */
    @ViewChild('trigger') trigger: CdkOverlayOrigin;

    // @ContentChild(NKD_SELECT_TRIGGER) customTrigger: MatSelectTrigger;

    constructor() { }

    /** Whether or not the overlay panel is open. */
    get panelOpen(): boolean {
        return this._panelOpen;
    }

    /** Toggles the overlay panel open or closed. */
    public toggle(): void {
        this.panelOpen ? this.close() : this.open();
    }

    /** Opens the overlay panel. */
    public open(): void {
        this._triggerRect = this.trigger.elementRef.nativeElement.getBoundingClientRect();
        this._panelOpen = true;
    }

    /** Closes the overlay panel and focuses the host element. */
    public close(): void {
        this._panelOpen = false;
    }

    public ngOnInit(): void {
        if (this.panelOpen) {
            this._triggerRect = this.trigger.elementRef.nativeElement.getBoundingClientRect();
        }
    }

}
