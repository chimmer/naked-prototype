import { Component, ContentChildren, HostBinding, HostListener, Input, OnInit } from '@angular/core';

@Component({
  selector: 'nkd-option',
  template: `
    <div class="nkd-option-value">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
// tslint:disable-next-line:component-class-suffix
export class NakedOption {

    @Input()
    public value: any;

    @HostListener('click')
    public onClick(): void {
        console.log('I have been clicked, my value is:', this.value);
    }

}
